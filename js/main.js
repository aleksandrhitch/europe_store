var swiper = new Swiper('.main-slider', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.main-slider-pagination',
        clickable: true,
    },
});

var swiper_renters = new Swiper('.renters-slider', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        clickable: true,
        el: '.swiper-pagination',
    },
});

var swiper_card = new Swiper('.card-slider', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        320: {
            slidesPerView: 1,

        },
        640: {
            slidesPerView: 1,

        },
        768: {
            slidesPerView: 1,

        },
        1024: {
            slidesPerView: 1,

        },
        1250: {
            slidesPerView: 1,

        },
    },
});

var swiper_card = new Swiper('.participants-mobile-slider', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        320: {
            slidesPerView: 1,

        },
        640: {
            slidesPerView: 1,

        },
        768: {
            slidesPerView: 1,

        },
        1024: {
            slidesPerView: 1,

        },
        1250: {
            slidesPerView: 1,

        },
    },
});

var swiper_card = new Swiper('.slider-similar-products', {
    spaceBetween: 30,
    initialSlide: 3,
    navigation: {
        nextEl: '.swiper-button-next-card',
        prevEl: '.swiper-button-prev-card',
    },
    pagination: {
        el: '.swiper-pagination-card',
        clickable: true,
    },
    breakpoints: {
        320: {
            slidesPerView: 1.3,
            centeredSlides: true,
        },
        640: {
            slidesPerView: 2,
            centeredSlides: true,
        },
        768: {
            slidesPerView: 2,
            centeredSlides: true,
        },
        1024: {
            slidesPerView: 3,

        },
        1250: {
            slidesPerView: 4,

        },
    },
});

var slider_images = new Swiper('.slider-images', {
    breakpoints: {
        320: {
            slidesPerView: 1,
        },
        640: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 4,
        },
        1024: {
            slidesPerView: 5,

        },
    },
    spaceBetween: 0,
    freeMode: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});


var swiper = new Swiper('.category-slider', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
        clickable: true,
    },
    breakpoints: {
        320: {
            slidesPerView: 3,
        },
        640: {
            slidesPerView: 4,
        },
        768: {
            slidesPerView: 5,
        },
        1024: {
            slidesPerView: 10,

        },
    }
});

$(document).ready(function () {

    $('.category-slider .swiper-slide').hover(function () {
            $(this).find('.slide-category-info').addClass('showInfo');
    }, function () {
          $(this).find('.slide-category-info').removeClass('showInfo');
    });

    if( $('.fancybox').length > 0 ) {
        $(".fancybox").fancybox();
    }

    // display burger menu

    $('.burger-button').on('click', function() {
        $(this).toggleClass('active');
        $('.burger-menu-layer').toggleClass('active');

    });

    // modals

    $('.header-lower-info a').on('click', function () {

        $('.modals').addClass('active');
        $('.call-back').addClass('active');

    });

    /**
     *
     * @param element - element for display
     * @param className - add this classname to element
     * @param bodyclass - add this classname to body element
     */
    function openModal(element, className) {
        $(''+element+'').addClass(''+className+'');
    }

    $('.title').on('click', function () {
        openModal('.modals', 'active');
        openModal('.call-back', 'active');
        $('body').addClass('no-scroll');
    });

    $('.button-inverse').on('click', function (e) {
        e.preventDefault();
        openModal('.modals', 'active');
        openModal('.order-modal', 'active');
        $('body').addClass('no-scroll');
    });

    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".modal-content"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            closeElement('.modals', 'active', true);
        }
    });

    /**
     * @param element - element for closing
     * @param classForRemove - class for removing from elements
     * @param stateForRemove - true or false, true = find element in DOM and will remove class from them
     */
    function closeElement(element, classForRemove, stateForRemove = false) {

        $(''+element+'').removeClass(''+classForRemove+'');
        if( stateForRemove == true ) {
            $(''+element+'').find('.'+classForRemove+'').removeClass(''+classForRemove+'');
        }
        $('body').removeClass('no-scroll');
    }

    $('.close').on('click', function () {
        closeElement('modals', 'active', true);
    });

    $('.minus').on('click', function () {

        let parent = $(this).parent()
        let elementValue = parent.find('.value');
        let inputValue = parent.find('.value_input');
        if( Number( elementValue.text() ) > 0 ) {
            elementValue.text( Number( elementValue.text() ) - 1 );
            inputValue.val(elementValue.text());
        }
    });

    $('.plus').on('click', function () {

        let parent = $(this).parent();
        let elementValue = parent.find('.value');
        let inputValue = parent.find('.value_input');
        elementValue.text( Number( elementValue.text() ) + 1 );
        inputValue.val(elementValue.text());

    });

    // tabs
    $('.tab-links a').on('click', function (e) {
        e.preventDefault();
        $('.tab-links a').removeClass('active-tab');
        $(this).addClass('active-tab');
        let elementId = $(this).attr('href');
        $('.tab-content-item').removeClass('active-tab');
        $(elementId).addClass('active-tab');
    });

    // filters
    $('.filter-title').on('click', function () {
        $(this).find('.filter-marker').toggleClass('opened')
        $(this).next().slideToggle();
    });

    // vertical accordion

    $('.accordion-vertical li').hover(function (){
        $('.accordion-vertical li').removeClass('active');
        $(this).addClass('active');
    }, function (){
         // $(this).removeClass('active');
    });
});






















